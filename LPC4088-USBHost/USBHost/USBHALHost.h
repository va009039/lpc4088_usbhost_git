#if defined(TARGET_NUCLEO_F401RE)
#include "USBHALHost_F401RE.h"
#elif defined(TARGET_KL46Z)||defined(TARGET_KL25Z)||defined(TARGET_K64F)
#include "USBHALHost_KL46Z.h"
#elif defined(TARGET_LPC4088)||defined(TARGET_LPC1768)
#include "USBHALHost_LPC4088.h"
#else
#error "target error"
#endif


